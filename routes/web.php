<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();

Route::get('/', 'WelcomeController@index')->name('welcome');

Route::group(['middleware' => "auth"], function() {
    // %%%%%%% Students Routes Start %%%%%%%%%
    Route::get('/home', 'StudentsController@index')->name('students.list');
    //Rodo visus studentus

    Route::get("/student/create", "StudentsController@create")->name('student.create');
    //iskviecia kontrolerio funkcija create  ir ji grazina studento sukurimo formos view'a

    Route::get("/grade/show/{id}", "GradesController@show")->name('grades.show');
    //iskviecia kontrolerio funkcija show su vienu parametru (id) ir parodo view`a grades.show
    //ir perduoda jam studenta (viena) surades pagal id

    Route::post("/student/destroy/{id}", "StudentsController@destroy")->name('student.destroy');
     //iskviecia kontrolerio funkcija destroy, o ji pagal id istrina studenta ir grazina viewa students.list
     //su zinute apie sekminga istrinima.

    Route::post("/student/update/{id}", "StudentsController@update")->name('student.update');
     //Atnaujina modelio duomenis ir iraso i DB, pagal id, zinoma :)

    Route::get("/student/edit/{id}", "StudentsController@edit")->name('student.edit');
    //Redaguoja studenta

    Route::post("/student/store", "StudentsController@store")->name('student.store'); //zmogu iraso i duombaze
    // %%%%%%% Students Routes End %%%%%%%%%

    // %%%%%%% Lectures Routes Start %%%%%%%%%
    Route::get('/lectures', 'LecturesController@index')->name('lectures.list');
    Route::get("/lecture/create", "LecturesController@create")->name('lecture.create');
    Route::post("/lecture/store", "LecturesController@store")->name('lecture.store');
    Route::post("/lecture/destroy/{id}", "LecturesController@destroy")->name('lecture.destroy');
    Route::get("/lecture/edit/{id}", "LecturesController@edit")->name('lecture.edit');
    Route::post("/lecture/update/{id}", "LecturesController@update")->name('lecture.update');
    // %%%%%%% Lectures Routes End %%%%%%%%%
    // %%%%%%% Grades Routes Start %%%%%%%%%
    Route::get('/grades', 'GradesController@index')->name('grades.list');
    Route::post("/grade/destroy", "GradesController@destroy")->name('grade.destroy');
    //Istrina pazymi is DB, bet id perduoda su requestu per post, taip pat perduoda is kur ateina tryinimas, kad galima butu teisingai grazinti i sarasa, vienas yra kur visi pazymiai aplamai, o kitas kur tik vieno studento visi pazymiai.

    Route::get("/grade/create", "GradesController@create")->name('grade.create'); //tik iskviecia pasirinkimo forma
    Route::get("/grade/create/for/{id}", "GradesController@createforone")->name('gradeforone.create');
    Route::post("/grade/store", "GradesController@store")->name('grade.store'); //Istrina studenta is DB
    // %%%%%%% Grades Routes Ends %%%%%%%%%
});
