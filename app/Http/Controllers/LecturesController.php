<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Lecture;

class LecturesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $lectures = Lecture::orderBy('name', "ASC")->get();
        $lecturesCount = Lecture::count();

        return view("lectures.index", [
                  "lectures" => $lectures,
                  "lecturesCount" => $lecturesCount,
              ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('lectures.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $messages = [
              'required' => 'Laukelis :attribute turi buti uzpildytas',
              'name.required' => 'Įveskite paskaitos pavadinimą!',
          ];

        $validatedData = $request->validate([
              'name' => 'required|max:64|min:3'
          ], $messages);

        $lecture = new Lecture();
        $lecture->name = $request->name;
        $lecture->description = $request->description;
        $lecture->save();

        return redirect()->route('lectures.list')->with('zinute', 'Paskaita sėkmingai pridėta ' . $request->name);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $lecture = Lecture::find($id);

        return view('lectures.edit', [ 'lecture' => $lecture ]); //reiketu naudoti vienaskaita
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Patikriname uzklausos duomenis
        $messages = [
                'required' => 'Laukelis :attribute turi buti uzpildytas'
            ];

        $validatedData = $request->validate([
                'name' => 'required|min:2',
                'description' => 'min:2',
            ], $messages);

        $lecture = Lecture::find($id);
        $lecture->name = $request->name;
        $lecture->description = $request->description;
        $lecture->save();

        return redirect()->route('lectures.list')->with('zinute', 'Paskaita ' . $request->name . ' sėkmingai atnaujinta ');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $lecture = Lecture::find($id);
        $lecture->delete();
        //  Session::flash( 'status', 'Naujiena sekminga istrinta' );
        return redirect()->route('lectures.list')->with('zinute', 'Paskaita '. $lecture->name . ' sėkmingai ištrinta');
    }
}
