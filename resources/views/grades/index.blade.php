@extends("layouts.app")

 @section('content')
@if (session('zinute'))
      <div class="alert alert-success mt-1">
          {{ session('zinute') }}
      </div>
@endif

<h3 class="mt-3">Visi studentai bendrai turi {{ $gradesCount }} pažymius</h3>
<table class="table table-striped mt-1">
  <thead>
    <tr>
      <th scope="col">Studentas id</th>
      <th scope="col">Paskaita id</th>
      <th scope="col">Pažimys</th>
      <th></th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    @foreach ($grades as $grade) {{-- is kontrolerio gaunam $lectures kuriame yra visos paskaitos ir einam per jas ciklu --}}
    <tr>
      <td>{{ $grade->student->name }} {{ $grade->student->surname }}</td>
      <td>{{ $grade->lecture->name }}</td>
      <td>{{ $grade->grade }}</td>
      <td>
          <a class="btn btn-info" href="{{-- route('grade.edit', $lecture->id) --}}">Redaguoti</a>
      </td>
      <td>
          <form action="{{ route('grade.destroy') }}" method="POST">
              {{ csrf_field() }}
              <input type="hidden" name="id" value="{{ $grade->id }}">
              <input type="hidden" name="from" value="visuStudentu">
              <input type="submit" class="btn btn-danger" value="Trinti">
          </form>
      </td>
    </tr>
    @endforeach
  </tbody>
</table>
<a class="btn btn-info ml-4" href="{{ route('grade.create') }}">Pridėti</a>
<a class="btn btn-info ml-4" href="{{ route('students.list') }}">Studentų sąrašas</a>
@endsection