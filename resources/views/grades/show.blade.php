@extends("layouts.app")

@section('content')
  @if (session('zinute'))
        <div class="alert alert-success mt-1">
            {{ session('zinute') }}
        </div>
  @endif
<h3 class="mt-3 ">
  <small>Pasirinktas studentas</small> {{ $student->name . ' ' . $student->surname }}
</h3>
{{---------------------------------------}}
<table class="table table-striped mt-1">
    <thead>
        <tr>
            <th scope="col">Paskaita</th>
            <th class="text-center">Įvertinimas</th>
            <th></th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        @foreach ($student->grades as $grade)
        <tr>
            <td>{{ $grade->lecture->name }}</td>
            <td class="text-center">{{ $grade->grade }}</td>
            <td>
                <a class="btn btn-info" href="{{-- route('student.edit', $student->id) --}}">Redaguoti</a>
            </td>
            <td>
                <form action="{{ route('grade.destroy') }}" method="POST">
                    {{ csrf_field() }}
                    <input type="hidden" name="id" value="{{ $grade->id }}">
                    <input type="hidden" name="from" value="vienoStudento">
                    <input type="submit" class="btn btn-danger" value="Trinti">
	        </form>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
<a class="btn btn-info ml-4" href="{{ route('gradeforone.create', $student->id) }}">Prideti pažimį</a>
{{---------------------------------------}}

@endsection

