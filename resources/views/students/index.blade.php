@extends("layouts.app")

@section('content')
@if  (session('zinute'))
<div class="alert alert-success mt-1">
    {{ session('zinute') }}
</div>
@endif
<p>Viso studentų {{ $studentsCount }}</p>
<table class="table table-striped mt-1">
    <thead>
        <tr>
            <th scope="col">Pavardė</th>
            <th scope="col">Vardas</th>
            <th scope="col">El.Paštas</th>
            <th scope="col">Telefonas</th>
            <th></th>
            <th></th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        @foreach  ($students as $student)
        <tr>
            <td>{{ $student->surname }}</td>
            <td>{{ $student->name }}</td>
            <td>{{ $student->email }}</td>
            <td>{{ $student->phone }}</td>
            <td>
                <a class="btn btn-info" href="{{ route('student.edit', $student->id) }}">Redaguoti</a>
            </td>
            <td>
                <form action="{{ route('student.destroy',$student->id) }}" method="POST">
                    {{ csrf_field() }}
                    <input type="submit" class="btn btn-danger" value="Trinti">
          </form>
            </td>
            <td>
                <a class="btn btn-info" href="{{ route('grades.show', $student->id) }}">Pažymiai</a>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
<a class="btn btn-info ml-4" href="{{ route('student.create') }}">Pridėti studentą</a>
<a class="btn btn-info ml-4" href="{{ route('lectures.list') }}">Paskaitos</a>
<a class="btn btn-info ml-4" href="{{ route('grades.list') }}">Visų pažymių sąrašas</a>
@endsection